import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Fire and Ice';
  resources = ['books', 'characters', 'houses'];

  onTitleChanged(newTitle: string) {
    this.title = newTitle;
  }
}
