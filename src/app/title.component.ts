import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: 'editable-title',
  templateUrl: './title.component.html',
  styles: [`h1 {
    text-align: center;
  }`]
})
export class TitleComponent {
  @Input() title = "";
  @Output() titleChanged = new EventEmitter<string>();
  editMode = false;

  enableEditMode() {
    this.editMode = true;
  }

  submitTitle(newTitle: string) {
    this.titleChanged.emit(newTitle);
    this.editMode = false;
  }
}